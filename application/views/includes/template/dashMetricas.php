<div class="row" style="margin:40px;">
    <div class="space-6"></div>

    <div class="col-sm-12 infobox-container">
        <div class="infobox infobox-green">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-cutlery"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number"><?= $this->db->get_where('pedidos',array('cajadiaria'=>$this->user->cajadiaria))->num_rows() ?></span>
                <div class="infobox-content">Pedidos</div>
            </div>
        </div>

        <div class="infobox infobox-green">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-cutlery"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number"><?= $this->db->query(' SELECT COUNT(id) as total from pedidos where mesas_id IS NOT NULL and cajadiaria = '.$this->user->cajadiaria)->row()->total ?></span>
                <div class="infobox-content">Mesas</div>
            </div>
        </div>

        <div class="infobox infobox-green">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-beer"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number"><?= $this->db->query(' SELECT COUNT(id) as total from pedidos where barras_id IS NOT NULL and cajadiaria = '.$this->user->cajadiaria)->row()->total ?></span>
                <div class="infobox-content">Barras</div>
            </div>
        </div>

        <div class="infobox infobox-green">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-motorcycle"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number"><?= $this->db->query(' SELECT COUNT(id) as total from pedidos where deliverys_id IS NOT NULL and cajadiaria = '.$this->user->cajadiaria)->row()->total ?></span>
                <div class="infobox-content">Deliverys</div>
            </div>
        </div>

        <div class="infobox infobox-blue">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-money"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number"><?= $this->db->query(' SELECT FORMAT(SUM(total_pedido),0,"de_DE") as total from pedidos where facturado = 0 and cajadiaria = '.$this->user->cajadiaria)->row()->total ?></span>
                <div class="infobox-content">Pedidos por cobrar</div>
            </div>
        </div>

        <div class="infobox infobox-blue">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-money"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number"><?= $this->db->query(' SELECT FORMAT(SUM(monto),0,"de_DE") as total from egresos where cajas_diarias_id = '.$this->user->cajadiaria)->row()->total ?></span>
                <div class="infobox-content">Total egresos</div>
            </div>
        </div>

        <div class="infobox infobox-blue">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-money"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number"><?= $this->db->query(' SELECT FORMAT(SUM(total_pedido),0,"de_DE") as total from pedidos where facturado = 1 and cajadiaria = '.$this->user->cajadiaria)->row()->total ?></span>
                <div class="infobox-content">Pedidos cobrados</div>
            </div>
        </div>
        <div class="infobox infobox-blue">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-money"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number"><?= $this->db->query(' SELECT FORMAT(SUM(total_pedido),0,"de_DE") as total from pedidos where cajadiaria = '.$this->user->cajadiaria)->row()->total ?></span>
                <div class="infobox-content">Total Pedidos</div> 
            </div>
        </div>
    </div> 
    <div class="vspace-12-sm"></div>

    <div class="col-sm-5">
        
    </div><!-- /.col -->
</div>


