<form action="" method="post" onsubmit="return sendSms()">
    <?php if(!empty($msj)): ?>
    <div class="alert alert-success"><?= $msj ?></div>
    <?php endif ?>
    <div class="container">
        <div class="panel panel-default" style="margin-top:40px;">
            <div class="panel-heading">
                <h1 class="panel-title">Enviar un SMS a todos los clientes registrados</h1>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="exampleInputEmail1"><b>Seleccione un Lote</b> <small>Cantidad de clientes disponibles para envío: <span style="color:red"><?= $clientes ?></span></small></label>
                    <?php 
                        $data = array();
                        for($i=0;$i<=round($clientes/300,0);$i++){
                            $data[$i*300] = ($i*300).'-'.(($i*300)+300);
                        }
                        echo form_dropdown('lote', $data, 0,'class="form-control" id="lote"');
                    ?>
                  </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Mensaje</label>
                    <textarea class="form-control" id="sms" required name="msj" placeholder="Escribe aquí tu mensaje" maxlength="160"></textarea>
                  </div>                                
                <center><button type="submit" class="btn btn-success"  id="boton">ENVIAR</button></center>
            </div>
        </div>
    </div>
</form>
<div class="container" style="display: none" id="log">    
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
              <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  Log
                </a>
              </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                <?= $log ?>
              </div>
            </div>
          </div>
        </div>
</div>
<script>
    function sendSms2(){
        $("#boton").hide();
        $.post('<?= base_url('admin/sendSMS') ?>',{sms:$("#sms").val()},function(data){
            $("#collapseOne .panel-body").html(data);
            $("#boton").show();
        });
        $("#accordion").show();
        return false;
    }
    function sendSms(){
        window.open('<?= base_url('admin/sendSMS') ?>?msj='+encodeURIComponent($("#sms").val())+'&lote='+$("#lote").val(),"Enviar sms","width=420,height=230,resizable,scrollbars=yes,status=1");
        return false;
    }
    function showLog(data){
        $("#log").show();
        $("#log .panel-body").html(data);
    }
</script>