<html>
    
    <body>
        <table style="margin-top:120px;">
        <tr>
            <td>
            <?php 
                $this->db->join('user','user.id = clientes.user_id');
                $cliente = $this->db->get_where('clientes',array('clientes.id'=>$venta->cliente))->row() 
            ?>
            <table style="font-size:12px; border:1px solid black; width:350px;">
                <tr>
                    <td>
                        <div style="padding:10px; ">
                            <div style=" font-size:12px;">
                                Fecha: <?= date("d/m/Y",strtotime($venta->fecha)) ?>
                                Cond de venta: <?= $this->db->get_where('tipotransaccion',array('id'=>$venta->transaccion))->row()->denominacion ?>
                            </div>
                            <div style=" font-size:12px;">
                                RUC o CI Nº: <?= $cliente->cedula_ruc ?>
                                Días: Días
                            </div>
                            <div style=" font-size:12px;">
                                <div style="display: inline-block;">Razón Social: <?= $cliente->nombre.' '.$cliente->apellido ?></div>                            
                            </div>
                        </div>
                    </td>
                </tr>
            </table>

            <table style=" font-size:12px;">
                <tr>
                    <td style="padding:2px; text-align:center; font-size:12px;">DESCRIPCION</td>
                    <td style="padding:2px; text-align:center; font-size:12px;">CANT.</td>
                    <td style="padding:2px; text-align:center; font-size:12px;">PRECIO</td>
                    <td style="padding:2px; text-align:center; font-size:12px;">IMPORTE</td>
                    <td style="padding:2px; text-align:center; font-size:12px;">IVA</td>
                </tr>
                <?php foreach($detalles->result() as $d): ?>
                    <tr>
                        <td style="padding:2px; text-align:center; font-size:12px; border:0px; text-align: left;"><?php
                                    $this->db->or_where('id',$d->producto);
                                    $this->db->or_where('codigo',$d->producto);
                                    echo cortar_palabras($this->db->get('productos')->row()->nombre_comercial,4); ?></td>
                        <td style="padding:2px; text-align:center; font-size:12px; border:0px;" align="right"><?= $d->cantidad ?></td>
                        <td style="padding:2px; text-align:center; font-size:12px; border:0px;" align="right"><?= number_format($d->precioventa,0,',','.') ?></td>
                        <td style="padding:2px; text-align:center; font-size:12px; border:0px;" align="right"><?= number_format($d->totalcondesc,0,',','.') ?></td>
                        <td style="padding:2px; text-align:center; font-size:12px; border:0px;" align="right"><?= number_format($d->iva,0,',','.') ?></td>
                    </tr>
                <?php endforeach ?>                                            
                <tr>
                    <td colspan="3" style="padding:2px; border:0px;"> <?= $this->enletras->ValorEnLetras($venta->total_venta,'Gs') ?> <br/>TOTAL: </td>
                    <td colspan="2" style="padding:2px; border:0px;"><br/><?= number_format($venta->total_venta,0,',','.').' Gs' ?></td>
                </tr>
                <tr>
                    <td style="padding:2px; border:0px;">
                        EXENTAS:  <?= number_format($venta->exenta,0,',','.') ?><br/>
                        IVA 5%: <?= number_format($venta->iva,0,',','.') ?>
                    </td>
                    <td colspan="2" style="padding:2px;">
                        GRAVADAS 5%:  0<br/>
                        IVA10%: <?= number_format($venta->iva2,0,',','.') ?>
                    </td>
                    <td style="padding:2px;">                                    
                        GRAVADAS 10%:  0<br/>
                        TOTAL IVA: <?= number_format($venta->total_iva,0,',','.') ?>
                    </td>
                </tr>
            </table>
    </td>

    <td style="padding-left:20px;">
            <table style="font-size:12px; border:1px solid black; width:350px;">
                <tr>
                    <td>
                        <div style="padding:10px; ">
                            <div style=" font-size:12px;">
                                Fecha: <?= date("d/m/Y",strtotime($venta->fecha)) ?>
                                Cond de venta: <?= $this->db->get_where('tipotransaccion',array('id'=>$venta->transaccion))->row()->denominacion ?>
                            </div>
                            <div style=" font-size:12px;">
                                RUC o CI Nº: <?= $cliente->cedula_ruc ?>
                                Días: Días
                            </div>
                            <div style=" font-size:12px;">
                                <div style="display: inline-block;">Razón Social: <?= $cliente->nombre.' '.$cliente->apellido ?></div>                            
                            </div>
                        </div>
                    </td>
                </tr>
            </table>

                <table style=" font-size:12px;">
                <tr>
                    <td style="padding:2px; text-align:center; font-size:12px;">DESCRIPCION</td>
                    <td style="padding:2px; text-align:center; font-size:12px;">CANT.</td>
                    <td style="padding:2px; text-align:center; font-size:12px;">PRECIO</td>
                    <td style="padding:2px; text-align:center; font-size:12px;">IMPORTE</td>
                    <td style="padding:2px; text-align:center; font-size:12px;">IVA</td>
                </tr>
                <?php foreach($detalles->result() as $d): ?>
                    <tr>
                        <td style="padding:2px; text-align:center; font-size:12px; border:0px; text-align: left;"><?php
                                    $this->db->or_where('id',$d->producto);
                                    $this->db->or_where('codigo',$d->producto);
                                    echo cortar_palabras($this->db->get('productos')->row()->nombre_comercial,4); ?></td>
                        <td style="padding:2px; text-align:center; font-size:12px; border:0px;" align="right"><?= $d->cantidad ?></td>
                        <td style="padding:2px; text-align:center; font-size:12px; border:0px;" align="right"><?= number_format($d->precioventa,0,',','.') ?></td>
                        <td style="padding:2px; text-align:center; font-size:12px; border:0px;" align="right"><?= number_format($d->totalcondesc,0,',','.') ?></td>
                        <td style="padding:2px; text-align:center; font-size:12px; border:0px;" align="right"><?= number_format($d->iva,0,',','.') ?></td>
                    </tr>
                <?php endforeach ?>                                            
                <tr>
                    <td colspan="3" style="padding:2px; border:0px;"> <?= $this->enletras->ValorEnLetras($venta->total_venta,'Gs') ?> <br/>TOTAL: </td>
                    <td colspan="2" style="padding:2px; border:0px;"><br/><?= number_format($venta->total_venta,0,',','.').' Gs' ?></td>
                </tr>
                <tr>
                    <td style="padding:2px; border:0px;">
                        EXENTAS:  <?= number_format($venta->exenta,0,',','.') ?><br/>
                        IVA 5%: <?= number_format($venta->iva,0,',','.') ?>
                    </td>
                    <td colspan="2" style="padding:2px;">
                        GRAVADAS 5%:  0<br/>
                        IVA10%: <?= number_format($venta->iva2,0,',','.') ?>
                    </td>
                    <td style="padding:2px;">                                    
                        GRAVADAS 10%:  0<br/>
                        TOTAL IVA: <?= number_format($venta->total_iva,0,',','.') ?>
                    </td>
                </tr>
            </table>
            </td>
        </tr>
    </table>
    </body>
</html>