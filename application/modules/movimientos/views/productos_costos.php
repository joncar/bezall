<?php echo $crud->header ?>
<?php echo $crud->output ?>
<script>
	$("#field-ingredientes_id").on('change',function(){
		$.post('<?= base_url('movimientos/productos/productos/json_list') ?>',{
			'search_text[]':$(this).val(),
			'search_field[]':"productos.id"
		},function(data){
			data = JSON.parse(data);
			if(data.length>0){
				$("#field-costo").val(data[0].precio_costo);
			}
		});
	});
	$("#field-cant, #field-costo").on("change",function(){
		var cant = parseFloat($("#field-cant").val());
		var costo = parseFloat($("#field-costo").val());
		$("#field-total_costo").val(cant*costo);
	});
</script>