<?php
	if(!empty($edit) && is_numeric($edit)){
		$nota = $this->elements->notas_credito_cliente(array('id'=>$edit));
	}
?>
<style>
	.panel{
		border-radius:0px;
	}

	.error{
		border: 1px solid red !important;
	}

	.patternCredito{
		background: #ffffffa6;width: 100%;height: 100%;position: absolute;top: 0px;left: 0px;
	}
</style>
<div class="panel panel-default">
	<div class="panel-heading">
		<h1 class="panel-title">
			<b>Datos generales</b>			
		</h1>
	</div>
	<div class="panel-body">
		<div class="row" style="position: relative;">
			<div class="col-xs-12 col-md-3">
				#Venta
				<input type="text" name="nro_venta" value="" placeholder="Inserte el numero de venta aquí" class="form-control">
			</div>
			<!--<div class="col-xs-12 col-md-2">
				#Nota
				<input type="text" name="nro_nota_credito" value="" placeholder="Inserte el numéro de control de nota de crédito" class="form-control">
			</div>-->
			<div class="col-xs-12 col-md-3">
				Actualizar stock
				<div class="form-control">
					<input type="radio" name="actualizar" value="0" <?= !empty($nota[0]) && $nota[0]->actualizar_stock=='0'?'checked=""':'' ?>> NO
					<input type="radio" name="actualizar" value="1" <?= empty($nota[0]) || $nota[0]->actualizar_stock=='1'?'checked=""':'' ?>> SI
				</div>
			</div>
			<div class="col-xs-12 col-md-3">
				Cliente
				<input type="text" name="cliente" value="" placeholder="Inserte el numero de venta" readonly="" class="form-control">
			</div>
			<div class="col-xs-12 col-md-3">
				Total Nota
				<input type="text" name="total_nota" value="0" placeholder="Inserte el numero de venta" readonly="" class="form-control">
			</div>
			
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-md-9" style="padding-right: 39px;">
		<div class="panel panel-default" style="border:0">
			<div style="height:208px; overflow-y: auto;">
				<table class="table table-bordered" id="ventaDescr">
					<thead>
						<tr>
							<th>Código</th>
							<th>Nombre</th>
							<th>Cantidad</th>
							<th>Precio</th>
							<th>Total</th>
						</tr>
					</thead>
					<tbody>

						<tr id="productoVacio">
							<td>
								<a href="javascript:void(0)" class="rem" style="display:none;color:red">
									<i class="fa fa-times"></i>
								</a> 
								<span>&nbsp;</span>
							</td>
							<td>&nbsp;</td>
							<td><input name="cantidad" class="cantidad" type="text" style="display:none; width:50px;text-align: right;padding: 0 6px;" value="0"></td>
							<td><input name="precio" class="precio" type="text" style="display:none; width:90px;text-align: right;padding: 0 6px;" value="0"></td>							
							<td>&nbsp;</td>
						</tr>

						
					</tbody>
				</table>
			</div>

			<div class="panel-footer">
				<div class="row">
					<div class="col-xs-12 col-md-2" style="text-align: center;padding: 6px;">
						Cant: <span id="cantidadProductos">0</span>					
					</div>
					<div class="col-xs-12 col-md-10" style="position: relative;">
						<i class="fa fa-search" data-toggle="modal" data-target="#inventarioModal" style="color: #0fcb0f;font-weight: bold;position: absolute;top: 10px;left: 21px;cursor:pointer;"></i>
						<input id="codigoAdd" type="text" class="form-control" placeholder="Código de producto" <?= empty($edit)?'readonly=""':'' ?> style="padding-left: 25px;padding-right: 73px;">
						<button style="position: absolute;top: 1px;right: 16px;padding: 1px;" class="btn btn-primary">Insertar</button>

						<div id="searchProductShort" style="display:none; background: #fff;width: 86%;position: absolute;z-index: 1;border: 1px solid gray;"><ul><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li></ul></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-md-3" style="padding-left: 5px;">
		<div class="panel panel-default">
			<div class="panel-heading">Resumen de nota</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12 col-md-4">Pesos: </div>
					<div class="col-xs-8" style="margin-bottom:5px">						
						<input type="text" name="total_pesos" value="0" readonly="" style="width: 100%;">
					</div>
					<div class="col-xs-12 col-md-4">Reales: </div>
					<div class="col-xs-8" style="margin-bottom:5px">						
						<input type="text" name="total_reales" value="0" readonly="" style="width: 100%;">
					</div>
					<div class="col-xs-12 col-md-4">Dolares: </div>
					<div class="col-xs-8" style="margin-bottom:5px">						
						<input type="text" name="total_dolares" value="0" readonly="" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>

		
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-md-9">
		<div class="btn-group btn-group-justified" role="group" aria-label="...">
		  <?php if(empty($edit)): ?>
		  <div class="btn-group" role="group">
		    <button type="button" class="btn btn-default btnNueva" onclick="nuevaNota();">Nueva nota</button>
		  </div>
		  <?php else: ?>
		  	<div class="btn-group" role="group">
		    	<a href="<?= base_url() ?>movimientos/ventas/notas_credito_cliente/add" class="btn btn-default btnNueva">Nueva nota</a>
		  	</div>
		  <?php endif ?>
		  <div class="btn-group" role="group">
		    <button type="button" class="btn btn-primary" onclick="save()">Guardar nota</button>
		  </div>
		  <div class="btn-group" role="group">
		    <button type="button" class="btn btn-default" <?= empty($edit)?'disabled="true"':'' ?>>Imprimir</button>
		  </div>
		</div>
	</div>
	<div class="col-xs-12 col-md-3" style="margin-top: -70px;">
		<div class="panel panel-default">
			<div class="panel-heading">Atajos</div>
			<div class="panel-body">
				<span>(ALT+C) <small>Enfocar busqueda por código</small></span><br/>
				<span>(ALT+I) <small>Mostrar busqueda avanzada</small></span><br/>
				<span>(ALT+P) <small>Procesar nota</small></span><br/>
				<span>(ALT+N) <small>Nueva nota</small></span><br/>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 respuestaVenta"></div>
</div>
<?php $this->load->view('_inventario_modal',array(),FALSE,'movimientos'); ?>
<script src="<?= base_url() ?>js/notasCredito.js"></script>
<script>
	<?php
		$ajustes = $this->db->get('ajustes')->row();
	?>
	var tasa_dolar = <?= $ajustes->tasa_dolares ?>;
	var tasa_real = <?= $ajustes->tasa_reales ?>;
	var tasa_peso = <?= $ajustes->tasa_pesos ?>;
	var codigo_balanza = <?= $ajustes->cod_balanza ?>;
	var vender_sin_stock = <?= $ajustes->vender_sin_stock ?>;
	var onsend = false;
	var nota = new NotaCredito();
	function initDatos(){
		nota.datos.sucursal = '<?= $this->user->sucursal ?>';
		nota.datos.caja = '<?= $this->user->caja ?>';
		nota.datos.cajadiaria = '<?= $this->user->cajadiaria ?>';
		nota.datos.fecha = '<?= date("Y-m-d H:i") ?>';	
		nota.datos.usuario = '<?= $this->user->id ?>';		
	}
	initDatos();
	nota.initEvents();
	//venta.updateFields();

	<?php 
		if(!empty($edit) && is_numeric($edit)): 			
	?>
		var edit = <?= count($nota)>0?json_encode($nota[0]):'{}' ?>;
		nota.datos = edit;
		nota.INP_nro_venta.val(edit.venta);
		nota.INP_nro_nota.val(edit.nro_nota_credito);
		console.log(nota.datos);
		nota.updateData();		
	<?php endif ?>

	function selCod(codigo){		
		nota.addProduct(codigo);		
		$("#inventarioModal").modal('toggle');
	}

	function save(){		
		if(!onsend){
			onsend = true;
			var datos =  JSON.parse(JSON.stringify(nota.datos));
			datos.productos = JSON.stringify(datos.productos);
			var accion = '<?= empty($edit)?'insert':'update/'.$edit  ?>';
			$("button").attr('disabled',true);
			insertar('movimientos/ventas/notas_credito_cliente/'+accion,datos,'.respuestaVenta',function(data){						
				var id = data.insert_primary_key;						
				var enlace = '';
				$(".respuestaVenta").removeClass('alert-info');
				//imprimir(id);
				//enlace = 'javascript:imprimir('+id+')';
				$('.respuestaVenta').removeClass('alert alert-danger').addClass('alert alert-success').html(data.success_message+'<p>La factura se mostrará automáticamente, si no lo hace puede pulsar <a href="'+enlace+'">este enlace</a></p>');
				$(".btnNueva").show();
				$(".btnNueva").attr('disabled',false);
			},function(){
				onsend = false;
				$("button").attr('disabled',false);
			});
		}	
	}

	function nuevaNota(){
		$("button").attr('disabled',false);
		nota.initNota();
	}
</script>