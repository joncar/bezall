<?php 
    require_once APPPATH.'/controllers/Panel.php';
    header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, ver");
    class Api extends Main{

    	protected $cookie = '';
    	protected $timeout = 30;
        function __construct() {
            parent::__construct();
        }

        function Login() {        
	        $url = base_url('main/login');
	        $ch = curl_init();    
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	        curl_setopt($ch, CURLOPT_URL, $url); 
	        $cookie = 'cookies.txt';
	        $timeout = 30;	        
	        curl_setopt($ch, CURLOPT_TIMEOUT,         10); 
	        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,  $timeout );
	        curl_setopt($ch, CURLOPT_COOKIEJAR,       $cookie);
	        curl_setopt($ch, CURLOPT_COOKIEFILE,      $cookie);
	        curl_setopt ($ch, CURLOPT_POST, 1);
	         curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
	        curl_setopt ($ch,CURLOPT_POSTFIELDS,"email=root@restaurant.com&pass=12345678");
	        $result = curl_exec($ch);
	        $this->cookie = $cookie;
	        curl_close($ch); 	        
	    }

        function consult($url,$data){
        	$url = base_url($url);
	        $ch = curl_init();            
	        curl_setopt($ch, CURLOPT_URL, $url);
	        @curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
	        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookie);
	        curl_setopt ($ch, CURLOPT_POST, 1);
	        curl_setopt($ch, CURLOPT_COOKIEFILE,$this->cookie);
	        curl_setopt ($ch,CURLOPT_POSTFIELDS,$data);
	         curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	        $result = curl_exec($ch);
	        curl_close($ch); 
	        return $result;

        }

        function test(){        	        	
        	
        	$url = base_url('main/login');
			$data = "email=root@restaurant.con&pass=12345678";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);	
			curl_setopt($ch, CURLOPT_POST,true);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
			curl_setopt($ch, CURLOPT_COOKIEJAR, "file.txt");
			curl_setopt($ch, CURLOPT_COOKIEFILE, "file.txt");	
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION,true);	
			if($html = curl_exec($ch) == false)
				echo 'Curl error: ' . curl_error($ch);
			else
			{
				curl_setopt($ch, CURLOPT_URL,base_url('panel'));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_COOKIEJAR, "file.txt");
				curl_setopt($ch, CURLOPT_COOKIEFILE, "file.txt");
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION,true);	
				
				if($html = curl_exec($ch) == false)
				{
					echo 'Curl error: ' . curl_error($ch);
				}
				else
				{
					echo 'Operation completed without any errors2';
				}
			}
		//	$html=curl_exec($ch);
			echo $html;
			exit;
        	
        }
        
    }
?>
