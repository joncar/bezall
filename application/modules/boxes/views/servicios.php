<?= $output->header ?>
<div style="text-align: right; margin:20px">
	<a href="<?= base_url('movimientos/ventas/ventas/add/'.$output->servicio) ?>" class="btn btn-success"><i class="fa fa-print"></i> Facturar</a>
</div>
<?= $output->output ?>
<script>
	$("#field-cantidad").on('change',function(){
		total();
	});
	$("#field-productos_id").on('change',function(){
		$.post('<?= base_url('movimientos/productos/productos/json_list') ?>',{
			'search_text[]':$(this).val(),
			'search_field[]':'id'
		},function(data){
			data = JSON.parse(data);
			if(data.length>0){
				$("#field-cantidad").val(1);
				$("#field-precio").val(data[0].precio_venta);
				total();
			}
		});
	});

	$('#crudForm').on('submit',function(){
		$.post('<?= base_url('boxes/admin/servicios/json_list') ?>',{
			'search_text[]':<?= $output->servicio ?>,
			'search_field[]':'id'
		},function(data){
			data = JSON.parse(data);
			if(data.length>0){
				$("#total").val(data[0].total);
			}
		});
		
	});

	function total(){
		var cantidad = parseFloat($("#field-cantidad").val());
		var precio = parseFloat($("#field-precio").val());
		$("#field-total").val(cantidad*precio);
	}
</script>