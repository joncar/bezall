<?php

require_once APPPATH.'/controllers/Panel.php';    

class Maestras extends Panel {

    function __construct() {
        parent::__construct();             
    }
    
    public function paises($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $output = $crud->render();
        $output->relatedlinks = array(array('admin/ciudades', 'Ciudades'));
        $this->loadView($output);
    }

    public function ciudades($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->set_relation('pais', 'paises', 'denominacion');
        $output = $crud->render();
        $output->relatedlinks = array(array('admin/ciudades', 'Ciudades'));
        $this->loadView($output);
    }
    
    public function proveedores($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->set_relation('ciudad', 'ciudades', 'denominacion');
        $crud->set_relation('pais_id', 'paises', 'denominacion');
        $crud->set_relation('tipo_proveedor', 'tipo_proveedores', 'denominacion');
        $crud->required_fields('denominacion', 'ruc', 'direccion', 'ciudad', 'pais_id', 'telefonofijo', 'telefax', 'celular', 'email');
        $crud->unset_fields('created', 'modified');
        $crud->field_type('usuario_id', 'hidden', $_SESSION['user']);
        $crud->columns('denominacion', 'tipo_proveedor', 'ruc', 'telefonofijo', 'celular', 'Observacion');
        $crud->unset_delete();
        $output = $crud->render();
        $this->loadView($output);
    }
    
    public function sucursales($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $crud->set_relation('pais_id', 'paises', 'denominacion');
        $crud->set_relation('ciudad_id', 'ciudades', 'denominacion');
        $crud->set_relation_dependency('ciudad_id', 'pais_id', 'pais');
        $crud->field_type('principal', 'true_false', array('0' => 'No', '1' => 'Si'));

        $crud->callback_before_insert(function($post) {
            if ($post['principal'] == 1)
                $this->db->update('sucursales', array('principal' => 0));
        });
        $crud->callback_before_update(function($post) {
            if ($post['principal'] == 1)
                $this->db->update('sucursales', array('principal' => 0));
        });
        $output = $crud->render();
        $this->loadView($output);
    }
    
    public function clientes($x = '', $y = '', $return = false) {
        $crud = parent::crud_function($x, $y); 
        $crud->set_relation('user_id','user','{nombre} {apellido}');
        $crud->field_type('created','hidden',date("Y-m-d"))
             ->field_type('modified','hidden',date("Y-m-d"));     
         $crud->display_as('user_id','Usuario');    
        $output = $crud->render();        
        $this->loadView($output);
    }
    
    public function monedas($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->field_type('primario', 'true_false', array('0' => 'No', '1' => 'Si'));
        $crud->unset_delete();
        $output = $crud->render();
        $output->crud = 'user';
        $this->loadView($output);
    }
    
    public function motivo_salida($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $this->loadView($crud->render());
    }

    public function motivo_entrada($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $this->loadView($crud->render());
    }

    public function tipo_proveedores($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $this->loadView($crud->render());
    }
    
    public function cuentas($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);        
        $output = $crud->render();
        $this->loadView($output);
    }  
    
    function condiciones(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Condiciones de pago');
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->title = 'Condiciones de pago';
            $this->loadView($crud);
        }
        function estados(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Estados de ocupacion');
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->title = 'Estados de ocupacion';
            $this->loadView($crud);
        }
        
        function empleados(){
            $crud = $this->crud_function('','');            
            $crud->set_relation('user_id','user','{nombre} {apellido}');
            $crud->set_subject('Empleado');
            $crud->display_as('user_id','Usuario');
            $crud->unset_delete();
            $crud = $crud->render();            
            $crud->title = 'Empleados';
            $this->loadView($crud);
        }
        
        function tipo_pedidos(){
            $crud = $this->crud_function('','');
            $crud->set_subject('tipo de pedidos');
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->title = 'Administrar de tipos de pedidos';
            $this->loadView($crud);
        }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
