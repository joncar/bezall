<?php
    $qry = $this->db->query("
        SELECT cajadiaria.fecha_cierre as Fecha,
        format(cajadiaria.monto_inicial,0, 'de_DE') as 'Monto Inicial',
        format((cajadiaria.total_credito + cajadiaria.total_contado + cajadiaria.total_pago_clientes ),0, 'de_DE') as Ingreso,
        cajadiaria.total_egreso as Egreso,
        cajadiaria.efectivoarendir as 'Efectivo a Rendir'
        FROM cajadiaria 
        WHERE cajadiaria.abierto=0
        LIMIT 10
    ");
?>
<div class="widget-color-dark widget-box ui-sortable-handle" data-id="4">
   
            <div class="widget-header">
                <h5 class="widget-title"><i class="ace-icon fa fa-list"></i> Últimos 10 arqueos</h5>

                <div class="widget-toolbar">
                    <div class="widget-menu">
                        <!--<a data-toggle="dropdown" data-action="settings" href="#">
                            <i class="ace-icon fa fa-bars"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right dropdown-light-blue dropdown-caret dropdown-closer">
                            <li>
                                <a href="#dropdown1" data-toggle="tab"><b>Año Lectivo</b></a>
                            </li>                            
                        </ul>-->
                    </div>

                    <!--<a class="orange2" data-action="fullscreen" href="#">
                        <i class="ace-icon fa fa-expand"></i>
                    </a>

                    <a data-action="reload" href="#">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>-->

                    <a data-action="collapse" href="#">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <!--<a data-action="close" href="#">
                        <i class="ace-icon fa fa-times"></i>
                    </a>-->
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main no-padding">
                    <div class="widget-main no-padding">
                        <?php sqlToHtml($qry); ?>

                    </div>
                </div>
            </div>
</div>
<script>

</script>
