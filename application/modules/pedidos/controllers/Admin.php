<?php

require_once APPPATH . '/controllers/Panel.php';

class Admin extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
            die();
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
            die();
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
            die();
        }            
    }

    function pedidos($x = '',$y = '') {
        $crud = $this->crud_function('', '');
        $crud->set_theme('crud');                
        $crud->field_type('user_id','hidden',$this->user->id)
             ->set_relation('clientes_id','clientes','user_id')                 
             ->set_relation('j3eb7f57f.user_id','user','{nombre} {apellido}')
             ->display_as('s0d0e1b62','Cliente')                         
             ->display_as('sucursales_id','Sucursal')
             ->columns('s0d0e1b62','fecha_pedido','sucursales_id','facturado','observacion');
        if($crud->getParameters()=='add'){
            $crud->field_type('anulado','hidden',0);
        }
        $crud->callback_field('clientes_id',function($val,$row){            
            $this->db->join('user','user.id = clientes.user_id');
            $this->db->select('clientes.id, user.cedula_ruc, user.nombre, user.apellido');
            return form_dropdown_from_query('clientes_id','clientes','id','cedula_ruc nombre apellido',$val,'id="field-clientes_id"');
        });
        $crud->set_lang_string('insert_success_message','Sus datos han sido almacenados con éxito <script>document.location.href="'.base_url('pedidos/admin/pedidos_detalles/{id}/add').'";</script>');
        $crud->set_lang_string('update_success_message','Sus datos han sido almacenados con éxito <script>document.location.href="'.base_url('pedidos/admin/pedidos_detalles/'.$y.'/add').'";</script>');
        $crud->add_action('Detalle','',base_url('pedidos/admin/pedidos_detalles').'/');
        $crud = $crud->render('','application/modules/pedidos/views');        
        $crud->title = 'Pedidos';        
        $this->loadView($crud);
    }           

    function pedidos_detalles($id,$y = '',$z = '',$p = ''){
        if(is_numeric($id) && $y!='detalles'){
            $crud = $this->crud_function('','');
            $crud->set_subject('Detalle de pedido');
            $crud->field_type('pedidos_id','hidden',$id);
            $crud->set_relation('productos_id','productos','{codigo} {nombre_comercial}')
                 ->set_relation('empleado_responsable','empleados','user_id')
                 ->set_field_upload('muestra_adjunto','files')
                 ->display_as('pedidos_estados_id','Estado del pedido')
                 ->display_as('productos_id','Producto')
                 ->display_as('tipos_pedidos_id','Tipo del pedido')
                 ->display_as('categoriaproducto_id','Categoria del producto')
                 ->unset_back_to_list();
            $crud->set_relation_dependency('productos_id','categoriaproducto_id','categoria_id');
            $success_message = 'Sus datos han sido almacenados con éxito <script>$("#filtering_form").submit(); sumartodo();</script>';
            $crud->set_lang_string('insert_success_message',$success_message);
            $crud->set_lang_string('update_success_message',$success_message);
            $crud->set_lang_string('delete_success_message',$success_message);
            $crud->callback_field('empleado_responsable',function($val){
                $this->db->join('user','user.id = empleados.user_id');
                $this->db->select('empleados.id, user.cedula_ruc, user.nombre, user.apellido');
                return form_dropdown_from_query('empleado_responsable','empleados','id','cedula_ruc nombre apellido',$val,'id="field-empleado_responsable"');
            });
            if($crud->getParameters()=='list'){
                redirect('pedidos/admin/pedidos_detalles/'.$id.'/add');
            }

            $crud = $crud->render();
            $crud->title = 'Detalle de pedidos';
            //Crud header
            $header = new ajax_grocery_crud();
            $header->set_theme('header_data')
                 ->set_subject('Pedido')
                 ->set_table('pedidos')
                 ->set_relation('clientes_id','clientes','user_id')                 
                 ->set_relation('j3eb7f57f.user_id','user','{nombre} {apellido}')
                 ->display_as('s0d0e1b62','Cliente')                         
                 ->display_as('sucursales_id','Sucursal')
                 ->columns('s0d0e1b62','fecha_pedido','sucursales_id','facturado','observacion','total_pedido')
                 ->callback_column('total_pedido',function($val,$row){
                    return $this->db->query('select COALESCE(sum(total)) as total from pedidos_detalles where pedidos_id = '.$row->id)->row()->total;
                 })
                 ->where('pedidos.id',$id)
                 ->unset_add()->unset_edit()->unset_read()->unset_print()->unset_export()->unset_delete()
                 ->set_url('pedidos/admin/pedidos/edit/'.$id);
            $header = $header->render(1);
            //Crud->detalles
            $detalles = $this->detalles($id);
            $crud->js_files = array_merge($crud->js_files,$detalles->js_files);   
            $crud->css_files = array_merge($crud->css_files,$detalles->css_files);
            $crud->output = $this->load->view('pedidos',array('pedido'=>$id,'detalles'=>$detalles,'header'=>$header,'crud'=>$crud),TRUE);
            $this->loadView($crud);
        }elseif($z == 'add'){
            redirect('pedidos/admin/pedidos_detalles/'.$id.'/add');
        }elseif($z == 'edit'){
            redirect('pedidos/admin/pedidos_detalles/'.$id.'/edit/'.$p);
        }elseif($y="detalles"){
            $this->detalles($id,'');
        }else{
            redirect('pedidos/admin/pedidos');
        }

    }

    function detalles($x,$y = 1){
        $this->as['detalles'] = 'pedidos_detalles';
        $crud = $this->crud_function('','');        
        $crud->where('pedidos_id',$x)
             ->columns('productos_id','cantidad','precio_venta','total')
             ->display_as('productos_id','Producto')
             ->set_relation('productos_id','productos','nombre_comercial');
        $crud->unset_read()->unset_print()->unset_export()
             ->set_url('pedidos/admin/pedidos_detalles/'.$x.'/detalles/');
        $success_message = 'Sus datos han sido almacenados con éxito <script>$("#filtering_form").submit(); sumartodo();</script>';
        $crud->set_lang_string('insert_success_message',$success_message);
        $crud->set_lang_string('update_success_message',$success_message);
        $crud->set_lang_string('delete_success_message',$success_message);
        $crud = $crud->render($y);
        return $crud;
    }

    function detalles_pedidos(){
        $this->as['detalles_pedidos'] = 'pedidos_detalles';
        $crud = $this->crud_function('','');        
        $crud->unset_add()
             ->unset_edit()
             ->unset_delete()
             ->unset_print()
             ->unset_export()
             ->unset_read();
        $crud->set_relation('empleado_responsable','empleados','user_id')
             ->set_relation('j06f512db.user_id','user','{nombre} {apellido}')
             ->set_relation('pedidos_id','pedidos','clientes_id')
             ->set_relation('j905f2bd2.clientes_id','clientes','user_id')             
             ->set_relation('j2079e470.user_id','user','{nombre} {apellido}');        
        $crud->columns('pedidos_id','s1fbeff3f','tipo_pedidos_id','descripcion_pedido','s7b0b814b','fecha_hora_entrega','pedidos_estados_id')
             ->display_as('s7b0b814b','Empleado responsable')
             ->display_as('s1fbeff3f','Cliente')
             ->display_as('pedidos_id','#Pedido.')
             ->display_as('tipos_pedidos_id','Tipo de pedido')
             ->display_as('pedidos_estados_id','Estado del pedido')
             ->add_action('<i class="fa fa-calendar"></i> Actividades','',base_url('pedidos/admin/actividades').'/')
             ->add_action('<i class="fa fa-book"></i> Insumos','',base_url('pedidos/admin/insumos').'/');
        $crud->callback_column('s905f2bd2',function($val,$row){
            return '<a href="'.base_url('pedidos/admin/pedidos_detalles/'.$row->pedidos_id.'/edit/'.$row->id).'">'.$row->id.'</a>';
        });
        $crud->callback_column('s312ac9ca',function($val,$row){
            switch($row->pedidos_estados_id){
                case '3': // Iniciado
                    return '<span class="label label-info">'.$val.'</span>';
                break;
                case '4': // En Proceso
                    return '<span class="label label-warning">'.$val.'</span>';
                break;
                case '5': // Facturado
                    return '<span class="label label-danger">'.$val.'</span>';
                break;
                case '6': // Entregado
                    return '<span class="label label-default">'.$val.'</span>';
                break;
                case '7': // Terminado
                    return '<span class="label label-success">'.$val.'</span>';
                break;
                default:                    
                return $val;                
            };
        });
        $crud = $crud->render();
        $crud->title = 'Detalle de pedidos';
        $this->loadView($crud);
    }

    function actividades($x){
        $this->as['actividades'] = 'pedidos_detalles_actividades';
        $crud = $this->crud_function('','');                
        $crud->field_type('pedidos_detalles_id','hidden',$x)
             ->unset_columns('pedidos_detalles_id')
             ->where('pedidos_detalles_id',$x)
             ->set_relation('empleado_id','empleados','user_id')
             ->set_relation('j473dea9e.user_id','user','{nombre} {apellido}');
        $crud->columns('s7cf72ac6','tarea','fecha_hora_inicio','fecha_hora_terminada')
             ->display_as('s7cf72ac6','Empleado');
        $crud->callback_field('empleado_id',function($val){
            $this->db->select('empleados.id, user.nombre, user.apellido');
            $this->db->join('user','user.id = empleados.user_id');
            return form_dropdown_from_query('empleado_id','empleados','id','nombre apellido',$val);
        });
        $crud = $crud->render();
        $crud->header = new ajax_grocery_crud();
        $crud->header->set_table('pedidos_detalles')
                                     ->set_subject('Detalle del pedido')
                                     ->set_theme('header_data')
                                     ->where('pedidos_detalles.id',$x);
        $crud->header->set_url('pedidos/admin/detalles_pedidos/');
        $crud->header = $crud->header->render(1)
                                     ->output;
        $crud->output = $this->load->view('actividades',array('crud'=>$crud),TRUE);
        $crud->title = 'Actividades';
        $this->loadView($crud);    
    }
    function insumos($x){
        $this->as['insumos'] = 'pedidos_insumos';   
        $pedido = $this->db->get_where('pedidos_detalles',array('id'=>$x))->row()->pedidos_id;
        $crud = $this->crud_function('','');                 
        $crud->set_relation('productos_id','productos','nombre_comercial',array('insumo'=>1))
             ->set_relation_dependency('categoriaproducto_id','productos_id','categoriaproducto_id');
        $crud->field_type('pedidos_id','hidden',$pedido)
             ->display_as('categoriaproducto_id','Categoria de producto')
             ->display_as('productos_id','Producto')
             ->unset_columns('pedidos_id')
             ->where('pedidos_id',$pedido);        
        $crud = $crud->render();
        $crud->header = new ajax_grocery_crud();
        $header = new ajax_grocery_crud();
        $header->set_theme('header_data')
             ->set_subject('Pedido')
             ->set_table('pedidos')
             ->set_relation('clientes_id','clientes','user_id')                 
             ->set_relation('j3eb7f57f.user_id','user','{nombre} {apellido}')
             ->display_as('s0d0e1b62','Cliente')                         
             ->display_as('sucursales_id','Sucursal')
             ->columns('s0d0e1b62','fecha_pedido','sucursales_id','facturado','observacion')
             ->where('pedidos.id',$pedido)
             ->unset_add()->unset_edit()->unset_read()->unset_print()->unset_export()->unset_delete()
             ->set_url('pedidos/admin/detalles_pedidos/');
        $crud->header = $header->render(1)->output;
        $crud->output = $this->load->view('insumos',array('crud'=>$crud),TRUE);
        $crud->title = 'Insumos';
        $this->loadView($crud);    
    }

}

?>
