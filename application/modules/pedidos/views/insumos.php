<?= $crud->header ?>
<?= $crud->output ?>
<script>
	$("#field-productos_id").on('change',function(){
		$.post('<?= base_url('movimientos/productos/productos/json_list') ?>',{
			'search_field[]':'id',
			'search_text[]':$(this).val()
		},function(data){
			data = JSON.parse(data);
			data = data[0];
			$("#field-cantidad").val(1);
			$("#field-precio_costo").val(data.precio_costo);
			$("#field-cantidad").trigger('change');
		});
	});
	$("#field-cantidad,#field-precio_costo").on('change',function(){
		var cantidad = parseFloat($("#field-cantidad").val());
		var precio = parseFloat($("#field-precio_costo").val());
		$("#field-total").val(precio*cantidad);
	});
</script>