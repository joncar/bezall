<div class="row">
	<?= $header->output ?>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-6">
		<?= $crud->output ?>
	</div>
	<div class="col-xs-12 col-sm-6">
		<?= $detalles->output ?>
	</div>
</div>
<script>
	$("#field-total").attr('readonly',true);
	$("#field-productos_id").on('change',function(){
		$.post('<?= base_url('movimientos/productos/productos/json_list') ?>',{
			'search_text[]':$(this).val(),
			'search_field[]':'productos.id'
		},function(data){
			data = JSON.parse(data);
			data = data[0];
			$("#field-precio_costo").val(data.precio_costo);
			$("#field-precio_venta").val(data.precio_venta);
			$("#field-cantidad").val(1);
			total();
		});
	});

	$("#field-precio_venta, #field-cantidad").on('change',function(){
		total();
	});

	function total(){
		var total = 0;
		total = parseFloat($("#field-precio_venta").val())*parseFloat($("#field-cantidad").val());
		$("#field-total").val(total);
	}

	function sumartodo(){
		var total = 0;
		$.post('<?= base_url('pedidos/admin/pedidos_detalles/'.$pedido.'/detalles/json_list') ?>',{},function(data){
			data = JSON.parse(data);
			for(var i in data){
				total+= parseFloat(data[i].total);
			}
			console.log(total);
			$("#total_pedido").val(total);
		});
	}

	sumartodo();
</script>